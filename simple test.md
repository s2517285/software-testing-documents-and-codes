![Alt text](image-1.png)
###### 返回的结果
<pre>
{
    "name": "Nick2",
    "email": "scycz4@outlook.com",
    "password": "$2b$08$gbcSzGS02FTFlbM2KjhAi.0LPrTW9mMbqtTs7w0EhzBLI2GrxkPbi",
    "address": "Somewhere 33",
    "role": "Admin",
    "_id": "656c5cb7c39363d907d67e71",
    "__v": 0
}
</pre>
<br>

![Alt text](image-3.png)
###### 登录成功后会返回一个token，当你使用程序的api时需要用到这个token
管理员Nick2的token：eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY1NmM1Y2I3YzM5MzYzZDkwN2Q2N2U3MSIsImlhdCI6MTcwMTYwMTMxMiwiZXhwIjoxNzAxNjg3NzEyfQ.IlOuj_lP0gm9WBKPS0wILdEWDBf08aB3PAvKqvC2FV0

##### 登录成功后，admin可以通过user的id获取该user的orders
![Alt text](image-4.png)

![Alt text](image-5.png)
##### 这个是在test前设置的一系列的用户