## Sample Testing-Planning Document


#### <span style="font-size:15px;color:#0000FF"> [Requirements] </span>
R1: software provides registration functions that users can create an account
R2: users can log in the system
R3: admin can get the lists of all the users
R4: admin can get specific user's information 
R5: admin can delete a normal user 
R6: user can get their own information 
R7: user can update their own information 
R8: admin can get orders of any user 
R9: user can get all orders of themselves 
R10: admin can get specific orders of a user 
R11: user can add new order 
R12: user can get the information about their own orders 
R13: user can update the information about orders 
R14: user can delete the information about orders 
R15: The Software should provide a real-time response to the users' request within 2 seconds 
R16: The Software should guarantee the basic work of the database


#### <span style="font-size:15px;color:#0000FF"> [Priority and Pre-requisites] </span>
* R1: it is a functional requirement, which should ensure the normal use of the function
  * This requirement have high priority, hence we need to consider two different T&A approaches
  * According to sensitive principle, developers would introduce errors which results in faults. Hence, we need to let those faults happen more frequently to make them easy to detect
  * The inputs and outputs of the system api:
    * Inputs: 
      * JSON format: email, password and other user information
    * Outputs: 
      * status: the status code of the response
      * message: error message if email is malformed or user already exists, new users' information if error doesn't exist
  * The sensitive principle suggests us to test exhaustively to checks every possible combination of inputs and checks the result conforms to the specification
* R2: it is a functional requirements as above
  * high priority and obeying sensitive principle
  * The inputs and outputs of the system api:
    * Inputs:
      * email, password
    * Outputs:
      * status: the status code of the response
      * message: error message if user does not exist or enter wrong password, success message with user object in JSON format and access token if user and password are correct
* R3: it is a functional requirements as above
  * high priority and obeying sensitive principle
  * The inputs and outputs of the system api:
    * Inputs:
      * user model in JSON format
    * Outputs:
      * status: the status code of the response
      * message: error message if user is not an admin, success message with a list of users in JSON format
* R4: it is a functional requirements as above
  * high priority and obeying sensitive principle
  * The inputs and outputs of the system api:
    * Inputs:
      * user model in JSON format
      * the ID of users we want to query
    * Outputs:
      * status: the status code of the response
      * message: error message if user is not an admin, success message with the found user in JSON format
* R5: it is a functional requirements as above
  * high priority and obeying sensitive principle
  * The inputs and outputs of the system api:
    * Inputs:
      * user model in JSON format
      * the ID of users we want to delete
    * Outputs:
      * status: the status code of the response
      * message: error message if user is not an admin or the user to delete is an admin, success message with deleted User object in JSON format
* R6: it is a functional requirements as above
  * high priority and obeying sensitive principle
  * The inputs and outputs of the system api:
    * Inputs:
      * user model in JSON format
    * Outputs:
      * status: the status code of the response
      * message: error message if user doesn't exist, success message with user's own information object in JSON format
* R7: it is a functional requirements as above
  * high priority and obeying sensitive principle
  * The inputs and outputs of the system api:
    * Inputs:
      * user model in JSON format
      * updated user model in JSON format
    * Outputs:
      * status: the status code of the response
      * message: error message if user doesn't exist, success message with updated user information object in JSON format
* R8: it is a functional requirements as above
  * high priority and obeying sensitive principle
  * The inputs and outputs of the system api:
    * Inputs:
      * user model in JSON format
      * the ID of user we want to query
    * Outputs:
      * status: the status code of the response
      * message: error message if user is not an admin, success message with a list of orders in JSON format
* R9: it is a functional requirements as above
  * high priority and obeying sensitive principle
  * The inputs and outputs of the system api:
    * Inputs:
      * user model in JSON format
    * Outputs:
      * status: the status code of the response
      * message: error message if user doesn't exist, success message with a list of orders in JSON format
* R10: it is a functional requirements as above
  * high priority and obeying sensitive principle
  * The inputs and outputs of the system api:
    * Inputs:
      * user model in JSON format
      * the ID of order we want to query
    * Outputs:
      * status: the status code of the response
      * message: error message if user is not an admin and the order's owner's ID is not equal to the ID of the user ID, or order doesn't exist, success message with the order object in JSON format
* R11: it is a functional requirements as above
  * high priority and obeying sensitive principle
  * The inputs and outputs of the system api:
    * Inputs:
      * user model in JSON format
      * order's information in JSON format
    * Outputs:
      * status: the status code of the response
      * message: error message if user doesn't exist, or order doesn't exist, success message with inserted order object in JSON format
* R12: it is a functional requirements as above
  * high priority and obeying sensitive principle
  * The inputs and outputs of the system api:
    * Inputs:
      * user model in JSON format
      * the ID of order
    * Outputs:
      * status: the status code of the response
      * message: error message if user is not an admin and user is not the order's owner, or order doesn't exist, success message with the order object in JSON format
* R13: it is a functional requirements as above
  * high priority and obeying sensitive principle
  * The inputs and outputs of the system api:
    * Inputs:
      * user model in JSON format
      * the ID of order
    * Outputs:
      * status: the status code of the response
      * message: error message if user is not an admin and user is not the order's owner, or order doesn't exist, success message with the updated order object in JSON format
* R14: it is a functional requirements as above
  * high priority and obeying sensitive principle
  * The inputs and outputs of the system api:
    * Inputs:
      * user model in JSON format
      * the ID of order
    * Outputs:
      * status: the status code of the response
      * message: error message if user is not an admin and user is not the order's owner, success message with the deleted order object in JSON format
* R15: This is a lower priority requirement, which is also a measurable attribute of the code:
  * We can only really test this for the completed system so this is a system level test that probably occurs late
  * To verify we will need some sort of synthetic data and the means to run tests on this
    * generating user and order model to test the api
    * designing the logging system to capture real performance
    * feeding collected data back into the early testing
* R16: This is a higher priority requirement, because it affects other components:
  * To verify we will need some sort of synthetic data and the means to run tests on this
    * generating user and order model to test databases

#### <span style="font-size:15px;color:#0000FF"> [Scaffolding and Instrument] </span>
* R1~R14:
  *  we need some scaffolding to generate legal user model and order models to store in the database or to be used in the test cases
  *  we also need scaffolding to generate different kinds of illegal user mode and order models to be used in the test cases
* R15: 
  * we need some scaffolding to generate legal user model and order models to store in the database or to be used in the test cases
  * we need to design instrument to monitor the response time of project API to help analysis
* R16:
  * we need some scaffolding to generate legal user model and order models to store in the database
* An instrument to check code coverage should be designed to inspect whether all the code has not been tested. 
* Jest "expect" will be used as "assert" to check whether the request we send will get the expected response. Hence, the input data and expected output should be defined as scaffolding.




#### <span style="font-size:15px;color:#0000FF"> [Process and Risk] </span>
Since all the requirements need quantities of test cases, the user models and order models generations should start at an early stage such as "Analysis and design" stage. Additionally, the requirements from R1 to R14 have no dependent relations, hence it is not necessary to wait for all the functionalities to be implemented to test. 

The instrument to monitor the response time can be implemented after unit testing and integration testing finished, since the corresponding requirements is aimed at monitoring performance of the system. 

However, it is possible that the simulated concurrent api tests are not representative for R15. For example, the concurrent api tests tends to be about queries instead of addition, deletion and update, while the actual distribution of the api tests has fewest query requests. Moreover, inadequate unit testing may cause unexpected expense and delays in integration testing. 