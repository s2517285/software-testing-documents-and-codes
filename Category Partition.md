# Category Partition Testing: Separate identification of values that characterize the input space from generation of combinations
### Note: both User Role and User ID are implicit in the access token
### Registration function
#### <span style="font-size:20px;color:#0000FF"> parameter: </span>
*   <span style="font-size:20px;color:#0000FF">Email</span>
       * empty              [error]
       * email in bad form [error]
       * email already exists &nbsp;&nbsp;&nbsp;&nbsp;[error]
       * email in good form 




*   <span style="font-size:20px;color:#0000FF">Password</span>
    * empty                              [error]
    * password with special characters  [single]
    * normal password 


### Login
#### <span style="font-size:20px;color:#0000FF"> parameter: </span>
* <span style="font-size:20px;color:#0000FF "> Email</span>
    * empty        [error]
    * email not found &nbsp;&nbsp; [error]
    * email found  


* <span style="font-size:20px;color:#0000FF "> Password </span>
  * empty    [error]
  * wrong password without special characters &nbsp;&nbsp; &nbsp; [error]
  * wrong password with special characters   [single]
  * correct password
  
### Get All Users
#### <span style="font-size:20px;color:#0000FF"> parameter: </span>
* <span style="font-size:20px;color:#0000FF "> User Role</span>
    * non-admin &nbsp; [error]
    * admin  


### Get Specific Users
#### <span style="font-size:20px;color:#0000FF"> parameter: </span>
* <span style="font-size:20px;color:#0000FF "> User Role </span>
    * non-admin &nbsp; [error]
    * admin  

* <span style="font-size:20px;color:#0000FF "> user ID</span>
    * non-existed ID &nbsp; 
    * existed ID  
  

### Delete a User
#### <span style="font-size:20px;color:#0000FF"> parameter: </span>
* <span style="font-size:20px;color:#0000FF "> User Role</span>
    * non-admin &nbsp; [error]
    * admin  

* <span style="font-size:20px;color:#0000FF "> Input User</span>
    * ID
       * non-existed ID &nbsp;   
       * existed ID     [property id]
    * Role
       * non-admin &nbsp; [if id]
       * admin    [if id]  [error]
  
### Get User's own Information
#### <span style="font-size:20px;color:#0000FF"> parameter: </span>
* <span style="font-size:20px;color:#0000FF "> User ID</span>
    * non-existed ID &nbsp;   
    * the user's ID &nbsp; 
    * other user's ID  

### Update User's own Information
#### <span style="font-size:20px;color:#0000FF"> parameter: </span>
* <span style="font-size:20px;color:#0000FF "> "New" attribute</span>
    * true &nbsp; 
    * false   [single]
* <span style="font-size:20px;color:#0000FF "> User ID</span>
    * the user's ID &nbsp; 
    * other user's ID  
* <span style="font-size:20px;color:#0000FF "> New Information</span>
    * empty &nbsp; [single]
    * information  
  
### Get User's own Information
#### <span style="font-size:20px;color:#0000FF"> parameter: </span>
* <span style="font-size:20px;color:#0000FF "> User ID</span>
    * non-existed ID &nbsp;   
    * the user's own ID &nbsp; 
    * other user's ID  
  
### Get any User's Information
#### <span style="font-size:20px;color:#0000FF"> parameter: </span>
* <span style="font-size:20px;color:#0000FF "> User Role</span>
    * non-admin &nbsp; 
    * admin    
* <span style="font-size:20px;color:#0000FF "> User ID</span>
    * non-existed ID &nbsp;   
    * the user's own ID &nbsp; 
    * other user's ID  

### Get Orders of any user
#### <span style="font-size:20px;color:#0000FF"> parameter: </span>
* <span style="font-size:20px;color:#0000FF "> User Role</span>
    * non-admin &nbsp; [error]
    * admin    
* <span style="font-size:20px;color:#0000FF "> User ID</span>
    * non-existed ID &nbsp;   
    * the user's ID &nbsp; 
    * other user's ID  
  
### Get Orders of the user
#### <span style="font-size:20px;color:#0000FF"> parameter: </span>
* <span style="font-size:20px;color:#0000FF "> User ID</span>
    * non-existed ID &nbsp;   
    * the user's ID &nbsp; 
    * other user's ID  
  
### Get an order of a user
#### <span style="font-size:20px;color:#0000FF"> parameter: </span>
* <span style="font-size:20px;color:#0000FF "> User Role</span>
    * non-admin &nbsp; [error]
    * admin    
* <span style="font-size:20px;color:#0000FF "> User ID</span>
    * non-existed ID &nbsp;  [error] 
    * the user's ID &nbsp; 
    * other user's ID  [error]
* <span style="font-size:20px;color:#0000FF "> Order ID</span>
    * non-existed order ID &nbsp;   [error]
    * the user's order ID &nbsp; 
    * other user's order ID  [error]
  

### Add an order
#### <span style="font-size:20px;color:#0000FF"> parameter: </span>
* <span style="font-size:20px;color:#0000FF "> User ID</span>
    * non-existed ID &nbsp;  [error] 
    * existed user's ID &nbsp; 
* <span style="font-size:20px;color:#0000FF "> Order's Information</span>
    * empty &nbsp;  
    * incomplete order information  
    * complete order information  

### Update an existing order
#### <span style="font-size:20px;color:#0000FF"> parameter: </span>
* <span style="font-size:20px;color:#0000FF "> User Role</span>
    * non-admin &nbsp; [error]
    * admin    
* <span style="font-size:20px;color:#0000FF "> User ID</span>
    * non-existed ID &nbsp;  [error] 
    * the user's ID &nbsp; 
    * other user's ID  [error]
* <span style="font-size:20px;color:#0000FF "> Order ID</span>
    * non-existed order ID &nbsp;   [error]
    * the user's order ID &nbsp; 
    * other user's order ID  [error]
  
